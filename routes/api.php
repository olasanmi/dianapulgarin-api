<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\User as UserResource;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
	// login request
	Route::group(['prefix' => 'auth'], function () {
   	    Route::post('login', 'AuthController@login');
   		Route::group(['middleware' => 'auth:api'], function() {
        	Route::post('logout', 'AuthController@logout');
		});
	});

    Route::get('filter/by/sede', 'VacancieController@filterBySede');

    // auth request
	Route::group(['middleware' => 'auth:api'], function() {
        Route::resource('CaseReal','CaseRealController');
        Route::delete('CaseReal/{id}','CaseRealController@destroy');
        Route::post('CaseReal/{id}','CaseRealController@update');

        Route::resource('CaseSuccess','CaseSuccessController');
        Route::delete('CaseSuccess/{id}','CaseSuccessController@destroy');
        Route::post('CaseSuccess/{id}','CaseSuccessController@update');

        Route::get('newsletter','NewsletterController@index');

        //categories
        Route::resource('category','CategoryController');

        //post
        Route::resource('post','PostController');
        Route::post('post/{id}','PostController@update');
        Route::post('posts/images','PostController@addImages');
        Route::get('posts/images/{id}','PostController@deleteImages');

        //payment
        Route::resource('payment','PaymentController');

        //counting
        Route::resource('counting','CountingController');
        Route::post('update/counting','CountingController@update');

        // vacancies
        Route::resource('vacancie', 'VacancieController');
	});

    // all request no auth
    Route::get('cases-real/actives','CaseRealController@getActives');
    Route::get('cases-success/actives','CaseSuccessController@getActives');

    //mail
    Route::post('send/contact','EmailController@sendEmailContact');
    Route::post('send/service','EmailController@sendEmailService');

    //newsletter
    Route::resource('newsletter','NewsletterController');

    //categories
    Route::get('list/categories','CategoryController@index');

    //post
    Route::get('post/filters/{id}','PostController@show');
    Route::get('list/post','PostController@index');
    Route::post('posts/like','PostController@like');
    Route::post('posts/comments','PostController@commentPost');

    //payment
    Route::post('store/payment','PaymentController@store');
    Route::post('send/payment','PaymentController@pay');
    Route::post('payment/token','PaymentController@generate');
    Route::post('payment/customer','PaymentController@generateCustomer');

    //feeds
    Route::get('feeds','HomeController@getFeed');

    //counting
    Route::get('counting-all','CountingController@index');

    //vacantes
    Route::get('vacancies-all','VacancieController@index');
    Route::post('vacancie-data','VacancieController@sendData');
});
