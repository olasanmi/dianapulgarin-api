<!DOCTYPE html>
<html style="position: relative; min-height: 100%;">
<head>
    <meta charset="utf-8">
    <title>Solicitud de vacante.</title>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap');
    </style>
</head>
<body style="font-family: 'Montserrat', sans-serif; margin: 0; margin-bottom: 40px;">
    <table style="width: 550px; margin: 0px auto">
        <thead>
            <th>
                <center>
                    <img width="300px" src="https://www.dradianapulgarin.com/images/logo.png">
                </center>
            </th>
        </thead>
        <tbody>
            <tr>
                <td>
                        <h3 style="color: #A18541; font-weight: 700; font-size: 14pt; text-align: center;">
                            Solicitud de vacante.
                        </h3>
                    <p style="text-align: center; margin-top: -15px !important; font-size: 12pt; text-transform: uppercase">
                        Hola, mi nombre es <b>{{$data['name']}} {{$data['lastName']}}</b>, les escribo con la finalidad de optar por el puesto de <b><span style="text-transform: uppercase">{{$data['vacancy']}}</span></b> en la sede: <b>{{$data['sede']}}</b>.
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <b><span style="font-size: 12pt; text-transform: uppercase">Teléfono:</span></b> <span style="font-size: 12pt">{{$data['phone']}}</span>
                    </center>
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <b><span style="font-size: 12pt; text-transform: uppercase">Correo:</span></b> <span style="font-size: 12pt">{{$data['email']}}</span>
                    </center>
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <b><span style="font-size: 12pt; text-transform: uppercase">Dirección:</span></b> <span style="font-size: 12pt">{{$data['address']}}</span>
                    </center>
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <b><span style="font-size: 12pt; text-transform: uppercase">Tiempo experiencia:</span></b> <span style="font-size: 12pt">{{$data['timeExperience']}}</span>
                    </center>
                </td>
            </tr>
            <br>
            <tr style="display: flex; align-items: center;justify-content: space-around; background-color: #202018; width: 100%; padding: 1.6rem 1rem;">
                <td style="width: 100%;">
                    <center>
                        <a style="padding-right: 1rem;" href="https://es-la.facebook.com/pg/DraDianaPulgarin" target="_blank">
                                <img src="https://www.dradianapulgarin.com/images/facebook.png" width="23px">
                            </a>
                        <a style="padding-right: 1rem;" href="https://www.instagram.com/dradianapulgarin" target="_blank">
                                <img src="https://www.dradianapulgarin.com/images/insta.png" width="23px">
                            </a>
                        <a style="padding-right: 1rem;" href="https://www.youtube.com/channel/UCmFFQqYvl8akrOAe9FpYGFA" target="_blank">
                                <img src="https://www.dradianapulgarin.com/images/youtube.png" width="23px">
                            </a>
                        <a href="https://twitter.com/DraDianaPul" target="_blank">
                                <img src="https://www.dradianapulgarin.com/images/social.png" width="23px">
                            </a>
                    </center>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
