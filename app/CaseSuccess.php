<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseSuccess extends Model
{
    //fillable
    protected $fillable = ['description', 'path', 'status', 'comparative_path'];
}
