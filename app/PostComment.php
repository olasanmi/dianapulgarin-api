<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    //fillable
    protected $fillable = ['description', 'client_name', 'client_email', 'post_id'];

    //relations
    public function Post() {
    	return $this->belongsTo('App\Post');
    }
}
