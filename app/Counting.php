<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counting extends Model
{
    //fillable
    protected $fillable = ['total', 'title', 'service'];
}
