<?php

namespace App\Http\Requests\Vacancie;

use Illuminate\Foundation\Http\FormRequest;

class SendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'address' => 'required',
            'email' => 'required|email',
            'file' => 'file|required|mimes:pdf|max:2048',
            'lastName' => 'required',
            'name' => 'required',
            'phone' => 'required|max:11',
            'timeExperience' => 'required',
            'vacancy' => 'required'
        ];
    }
}
