<?php

namespace App\Http\Controllers;

use App\Vacancie;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Http\Requests\Vacancie\VacancieRequest;
use App\Http\Requests\Vacancie\SendRequest;
use App\Mail\SendVacancieData;
use Illuminate\Support\Facades\Mail;

class VacancieController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {
            $vacancies = Vacancie::where('status', 'LIKE', '%'.$request->status.'%')->get();
            return $this->successResponse($vacancies);
        } catch (\Exception $e) {
            return $this->errorUnprocessableEntityResponse($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VacancieRequest $request)
    {
        //
        try {
            $vacancie = Vacancie::create($request->all());
            return $this->successCreatedResponse('Se ha creado la vacante correctamente.');
        } catch (\Exception $th) {
            return $this->errorUnprocessableEntityResponse($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vacancie $vacancie)
    {
        //
        try {
            return $this->successCreatedResponse($vacancie);
        } catch (\Exception $th) {
            return $this->errorUnprocessableEntityResponse($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacancie $vacancie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacancie $vacancie)
    {
        //
        try {
            $vacancie->update($request->all());
            return $this->successCreatedResponse('Se ha modificado la vacante correctamente.');
        } catch (\Exception $th) {
            return $this->errorUnprocessableEntityResponse($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacancie $vacancie)
    {
        //
        try {
            $vacancie->delete();
            return $this->successCreatedResponse('Se ha eliminado la vacante correctamente.');
        } catch (\Exception $th) {
            return $this->errorUnprocessableEntityResponse($e->getMessage());
        }
    }

    /**
     *
     * send data por email talentohumano@dianapulgarin.com
     */
    public function sendData(SendRequest $request) {
        try {
            Mail::to('maalhequi95@gmail.com')->send(new SendVacancieData($request->all()));
            $this->successResponse('Se ha enviado el correo correctamente.');
        } catch (\Exception $e) {
            return $this->errorUnprocessableEntityResponse($e->getMessage());
        }
    }

    /**
     *
     * filter by sede
     */
    public function filterBySede(Request $request) {
        try {
            $vacancies = Vacancie::where('status', true)->where('sede', $request->sede)->get();
            return $this->successResponse($vacancies);
        } catch (\Exception $e) {
            return $this->errorUnprocessableEntityResponse($e->getMessage());
        }
    }
}
