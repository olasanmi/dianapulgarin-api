<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

use App\Http\Resources\Payment\PaymentCollection;

use Response;

use App\Http\Controllers\epayco\Epayco;
use App\Http\Controllers\epayco\Client;
use App\Http\Controllers\epayco\Utils\PaycoAes;

use App\Mail\Payment\PaymentEmail;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    private $tokens;

    public function __construct(){
        $this->tokens = '';
        //primer epaico con credenciales de marcos
        //segundo epaico con credenciales de juan
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all Payment
        $payments = Payment::orderBy('id', 'DESC')
        ->get();
        $response = Response::make(json_encode(['success' => new PaymentCollection($payments)]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store pago
        try {
            $payment = Payment::create([
                'amount' => $request->amount,
                'name' => $request->name . ' ' .$request->lastName,
                'doc' => $request->docType.'-'.$request->dni,
                'phone' => $request->phone,
                'address' => $request->address,
                'email' => $request->email,
                'ref' => $request->reference,
                'status' => 0,
                'card_card' => $request->cardNumber,
                'card_ccv' => $request->cardCvv,
                'card_year' => $request->cardDate,
                'place' => $request->placeName
            ]);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //get paiment data
        $url = 'https://ws.tupago.net.co/sandbox/getOrder/cit-dianap-2021';
        $curl = curl_init("$url");
        error_log(var_export($curl));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'x-api-key: ' . 'MzFFNjYyREQ0RTFCLUM0M0YtNDE0My05NjkwLUZCNUJBRTcxRTQxRg==',
        ));
        $response = curl_exec($curl);
        if (curl_errno($ch)) {
            return $error_msg = curl_error($ch);
        }
        curl_close($curl);
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

    public function generate(Request $request) {
        try {
            if ($request->sede == 'Medellín' or $request->sede == 'Cali') {
                $epayco = new Epayco(array(
                    "apiKey" => "0d56a6a811396f52e0f467c74845a373",
                    "privateKey" => "96a188e77aaaed53bb21ede9c36fab7d",
                    "lenguage" => "ES",
                    "test" => false
                ));
                $token = $epayco->token->create(array(
                    "card[number]" => $request->number,
                    "card[exp_year]" => '20'. $request->yy,
                    "card[exp_month]" => $request->mm,
                    "card[cvc]" => $request->cvv
                ));

            } else {
                $epayco2 = new Epayco(array(
                    "apiKey" => "b990a756486231c81913c55f4d964827",
                    "privateKey" => "5fffe38c6c3c29728078317f60c38e34",
                    "lenguage" => "ES",
                    "test" => false
                ));
                $token = $epayco2->token->create(array(
                    "card[number]" => $request->number,
                    "card[exp_year]" => '20'. $request->yy,
                    "card[exp_month]" => $request->mm,
                    "card[cvc]" => $request->cvv
                ));
            }
            $this->token = $token;

            return response()->json(['token' => $this->token]);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    public function generateCustomer(Request $request) {
        try {
            if ($request->sede == 'Medellín' or $request->sede == 'Cali') {
                $epayco = new Epayco(array(
                    "apiKey" => "0d56a6a811396f52e0f467c74845a373",
                    "privateKey" => "96a188e77aaaed53bb21ede9c36fab7d",
                    "lenguage" => "ES",
                    "test" => false
                ));
                $customer = $epayco->customer->create(array(
                    'token_card' => $request->token,
                    "name" => $request->name .' '.$request->lastName,
                    "email" => $request->email,
                    "default" => true,
                    "city" => $request->sede,
                    "address" => $request->address,
                    "phone" => $request->phone,
                    "cell_phone"=> $request->phone
                ));
            } else {
                $epayco2 = new Epayco(array(
                    "apiKey" => "b990a756486231c81913c55f4d964827",
                    "privateKey" => "5fffe38c6c3c29728078317f60c38e34",
                    "lenguage" => "ES",
                    "test" => false
                ));
                $customer = $epayco2->customer->create(array(
                    'token_card' => $request->token,
                    "name" => $request->name .' '.$request->lastName,
                    "email" => $request->email,
                    "default" => true,
                    "city" => $request->sede,
                    "address" => $request->address,
                    "phone" => $request->phone,
                    "cell_phone"=> $request->phone
                ));
            }

            return response()->json(['customer' => $customer]);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }

    }

    public function pay(Request $request) {
        try {
            //generate reference
            $key = '';
            $keys = array_merge(range(0, 6), range('A', 'Z'));
            for ($i = 0; $i < 6; $i++) {
                $key .= $keys[array_rand($keys)];
            }
            if ($request->placeName == 'Medellín' or $request->placeName == 'Cali') {
                $epayco = new Epayco(array(
                    "apiKey" => "0d56a6a811396f52e0f467c74845a373",
                    "privateKey" => "96a188e77aaaed53bb21ede9c36fab7d",
                    "lenguage" => "ES",
                    "test" => false
                ));
                $pay = $epayco->charge->create(array(
                    "token_card" => $request->token,
                    "name" => $request->name,
                    "last_name" => $request->lastName,
                    "customer_id" => $request->customer,
                    "doc_type" => $request->docType,
                    "doc_number" => $request->dni,
                    "email" => $request->email,
                    "bill" => $key,
                    "description" => $request->description,
                    "value" => (float)$request->amount,
                    "tax" => 0,
                    "tax_base" => 0,
                    "currency" => "COP",
                    "dues" => "12",
                    "address" => $request->address,
                    "phone"=> $request->phone,
                    "cell_phone"=> $request->phone
                ));
            } else {
                $epayco2 = new Epayco(array(
                    "apiKey" => "b990a756486231c81913c55f4d964827",
                    "privateKey" => "5fffe38c6c3c29728078317f60c38e34",
                    "lenguage" => "ES",
                    "test" => false
                ));
                $pay = $epayco2->charge->create(array(
                    "token_card" => $request->token,
                    "name" => $request->name,
                    "last_name" => $request->lastName,
                    "customer_id" => $request->customer,
                    "doc_type" => $request->docType,
                    "doc_number" => $request->dni,
                    "email" => $request->email,
                    "bill" => $key,
                    "description" => $request->description,
                    "value" => (float)$request->amount,
                    "tax" => "0",
                    "tax_base" => "0",
                    "currency" => "COP",
                    "dues" => "12",
                    "address" => $request->address,
                    "phone"=> $request->phone,
                    "cell_phone"=> $request->phone
                ));
            }

            if ($pay->status == true and $pay->success == true) {
                if ($pay->data->estado == 'Rechazada') {
                    $payment = Payment::create([
                        'amount' => $request->amount,
                        'name' => $request->name . ' ' . $request->lastName,
                        'doc' => $request->docType.'-'.$request->dni,
                        'phone' => $request->phone,
                        'address' => $request->address,
                        'email' => $request->email,
                        'ref' => $key,
                        'status' => 0,
                        'card_card' => $request->cardNumber,
                        'card_ccv' => $request->cardCvv,
                        'card_year' => $request->cardDate,
                        'place' => $request->placeName
                    ]);
                    Mail::to('auxcontablemedellin@dianapulgarin.com')->send(new PaymentEmail($payment));
                    $response = ['error' => 'Tu pago ha sido rechazado. Referencia: '. $payment->ref];
                    return response($response, 200);
                }
                if ($pay->data->estado == 'Fallida') {
                    $payment = Payment::create([
                        'amount' => $request->amount,
                        'name' => $request->name . ' ' . $request->lastName,
                        'doc' => $request->docType.'-'.$request->dni,
                        'phone' => $request->phone,
                        'address' => $request->address,
                        'email' => $request->email,
                        'ref' => $key,
                        'status' => 0,
                        'card_card' => $request->cardNumber,
                        'card_ccv' => $request->cardCvv,
                        'card_year' => $request->cardDate,
                        'place' => $request->placeName
                    ]);
                    Mail::to('auxcontablemedellin@dianapulgarin.com')->send(new PaymentEmail($payment));
                    $response = ['error' => 'Tu pago ha sido rechazado. Referencia: '. $payment->ref];
                    return response($response, 200);
                }
                if ($pay->data->estado == 'Pendiente') {
                    $payment = Payment::create([
                        'amount' => $request->amount,
                        'name' => $request->name . ' ' . $request->lastName,
                        'doc' => $request->docType.'-'.$request->dni,
                        'phone' => $request->phone,
                        'address' => $request->address,
                        'email' => $request->email,
                        'ref' => $key,
                        'status' => 0,
                        'card_card' => $request->cardNumber,
                        'card_ccv' => $request->cardCvv,
                        'card_year' => $request->cardDate,
                        'place' => $request->placeName
                    ]);
                    Mail::to('auxcontablemedellin@dianapulgarin.com')->send(new PaymentEmail($payment));
                    $response = ['error' => 'Tu pago se encuentra pendiente. Referencia: '. $payment->ref];
                    return response($response, 200);
                }
                if ($pay->data->estado == 'Aceptada') {
                    $payment = Payment::create([
                        'amount' => $request->amount,
                        'name' => $request->name . ' ' . $request->lastName,
                        'doc' => $request->docType.'-'.$request->dni,
                        'phone' => $request->phone,
                        'address' => $request->address,
                        'email' => $request->email,
                        'ref' => $key,
                        'status' => 1,
                        'card_card' => $request->cardNumber,
                        'card_ccv' => $request->cardCvv,
                        'card_year' => $request->cardDate,
                        'place' => $request->placeName
                    ]);
                    Mail::to('auxcontablemedellin@dianapulgarin.com')->send(new PaymentEmail($payment));
                    $response = ['success' => 'Se ha procesado tu pago correctamente. Referencia: '. $payment->ref];
                    return response($response, 200);
                }
            }

            if ($pay->status == false) {
                $response = ['errors' => array(['code' => 422, 'message'=> $pay->data->errors])];
                return response($response, 422);
            }

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    public function validateCheckout(Request $request) {
        if (isset($request->ref_payco)) {
            $response = Http::get('https://secure.epayco.co/validation/v1/reference/' . $request->ref_payco);
            $body = $response->json();
            $payment = Payment::where('ref', $body['data']['x_id_invoice'])
            ->first();
            switch ((int) $body['data']['x_cod_response']) {
                case 1:
                $payment->status = 1;
                $payment->save();
                break;

                case 2:
                $payment->status = 0;
                $payment->save();
                break;

                case 3:
                $payment->status = 0;
                $payment->save();
                break;

                case 4:
                $payment->status = 0;
                $payment->save();
                break;
            }
            return view('payments.response');
        }
    }

    public function confirmCheckout(Request $request) {
        if (isset($request->pasarela)) {
            if ($request->pasarela === 1) {
                $p_cust_id_cliente =  46667;
                $p_key = 'f792da5d9b6736301948ba7b12c3694c5e790cd7';
            }
        }
        $x_ref_payco      = $_REQUEST['x_ref_payco'];
        $x_transaction_id = $_REQUEST['x_transaction_id'];
        $x_amount         = $_REQUEST['x_amount'];
        $x_currency_code  = $_REQUEST['x_currency_code'];
        $x_signature      = $_REQUEST['x_signature'];
        $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
        $payment = Payment::where('ref', $_REQUEST['x_id_invoice'])->first();
        $numOrder = $payment->reference;
        $valueOrder = $payment->amount;
        $x_response     = $_REQUEST['x_response'];
        $x_motivo       = $_REQUEST['x_response_reason_text'];
        $x_id_invoice   = $_REQUEST['x_id_invoice'];
        $x_autorizacion = $_REQUEST['x_approval_code'];
        // se valida que el número de orden y el valor coincidan con los valores recibidos
        if ($x_id_invoice === $numOrder && $x_amount === $valueOrder) {
            if ($x_signature == $signature) {
                $x_cod_response = $_REQUEST['x_cod_response'];
                switch ((int) $x_cod_response) {
                    case 1:
                        $payment->status = 1;
                        $payment->save();
                    break;

                    case 2:
                        $payment->status = 0;
                        $payment->save();
                    break;

                    case 3:
                        $payment->status = 0;
                        $payment->save();
                    break;

                    case 4:
                        $payment->status = 0;
                        $payment->save();
                    break;
                }
            } else {
                die('Firma no válida');
            }
        } else {
            die('número de orden o valor pagado no coinciden');
        }
    }

}
