<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostImg;
use App\PostComment;
use Illuminate\Http\Request;

use App\Http\Resources\Post\PostCollection;
use App\Http\Resources\Post\PostResource;

use App\Helpers\Helper;

use Response;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all post
        $posts = Post::orderBy('id', 'DESC')
        ->get();
        $response = Response::make(json_encode(['success' => new PostCollection($posts)]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //store post
            $post = Post::create([
                'title' => $request->title,
                'description' => $request->description,
                'title_en' => $request->title_en,
                'description_en' => $request->description_en,
                'category_id' => $request->category
            ]);
            //set video url
            if ($request->has('urlVideo')) {
                $post->video_url = $request->urlVideo;
            }
            //set header image
            if ($request->has('file')) {
                $path = Helper::uploadImage($request->file, 'post');
                $post->header_img = $path;
            }
            //save image and video url if isset
            $post->save();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::findOrFail($id);
        $post->views = $post->views + 1;
        $post->save();
        $response = Response::make(json_encode(['success' => new PostResource($post)]), 200)->header('Content-Type','application/json');
        return $response;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            //update post
            $post = Post::findOrFail($request->id);
            $post->title = $request->title;
            $post->description = $request->description;
            $post->title_en = $request->title_en;
            $post->description_en = $request->description_en;
            $post->category_id = $request->category;
            //set video url
            if ($request->has('urlVideo')) {
                $post->video_url = $request->urlVideo;
            }
            if ($request->has('file')) {
                Helper::deleteFile($post->header_img, 'post');
                $path = Helper::uploadImage($request->file, 'post');
                $post->header_img = $path;
            }
            $post->save();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        try {
            if (isset($post->header_img)) {
                Helper::deleteFile($post->header_img, 'post');
            }
            $post->delete();
            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //add images to post
    public function addImages(Request $request){
        try {
            //update post
            $post = Post::findOrFail($request->id);
            if ($request->has('file')) {
                $path = Helper::uploadImage($request->file, 'post');
            }
            PostImg::create([
                'path' => $path,
                'post_id' => $request->id
            ]);

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //delete images to post
    public function deleteImages($id){
        try {
            //update post
            $post = PostImg::findOrFail($id);
            Helper::deleteFile($post->path, 'post');
            $post->delete();
            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //comment post
    public function commentPost(Request $request) {
        try {
           $comment = PostComment::create([
                'description' => $request->description,
                'post_id' => $request->id,
            ]);

            if ($comment) {
                $post = Post::findOrFail($request->id);
                $response = Response::make(json_encode(['success' => new PostResource($post)]), 200)->header('Content-Type','application/json');
                return $response;

            }
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //like post
    public function like(Request $request) {
        try {
            $post = Post::findOrFail($request->id);
            $post->likes = $post->likes + 1;
            $post->save();
            $response = Response::make(json_encode(['success' => new PostResource($post)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
