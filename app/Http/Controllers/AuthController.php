<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Http\Resources\User\UserResource;

use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    // login
    public function login (Request $request) {
     	try {
     		$user = User::where('name', $request->name)->first();
     		if ($user) {
		        if (Hash::check($request->password, $user->password)) {
		            $token = $user->createToken('USER AUTH TOKEN')->accessToken;
		            $response = ['token' => $token, 'user' => new UserResource($user)];
		            return response($response, 200);
		        } else {
		            $response = ['errors' => array(['code' => 422, 'message'=>'La contraseña es incorrecta.'])];
		            return response($response, 422);
		        }
		    } else {
		        $response = ['errors' => array(['code' => 422, 'message' => 'El usuario no existe.'])];
		        return response($response, 422);
		    }

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

     public function logout (Request $request) {
     	try {
     		$token = $request->user()->token();
		    $token->revoke();
		    $response = ['message' => 'Has cerrado sesión correctamente.'];
		    return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
	}
}
