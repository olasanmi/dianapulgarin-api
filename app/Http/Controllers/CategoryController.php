<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Resources\Category\CategoryCollection;

use Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all categories
        $categories = Category::orderBy('position', 'ASC')
        ->get();
        $response = Response::make(json_encode(['success' => new CategoryCollection($categories)]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //add categories
        try {
            $category = Category::create([
                'name' => $request->name,
                'position' => $request->position
            ]);
            
            if ($category) {
                $response = $this->index();
                return $response;
            } else {
                return response()->json(['errors' => array(['code' => 422, 'message' => 'Error agregando la categoría'])], 422);
            }

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->delete()) {
            $response = $this->index();
            return $response;
        }
    }
}
