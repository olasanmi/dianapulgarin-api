<?php
 
 namespace App\Http\Controllers\epayco;
 use App\Http\Controllers\epayco\Client;
/**
 * Resource methods
 */
 class Resource extends Client
 {
    protected $epayco;
    /**
      * Instance epayco class
      * @param array $epayco
     */
     public function __construct($epayco)
    {
        $this->epayco = $epayco;
    }
 }