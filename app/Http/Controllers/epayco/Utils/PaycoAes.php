<?php

namespace App\Http\Controllers\epayco\Utils;

use App\Http\Controllers\epayco\Utils\McryptEncrypt;
use App\Http\Controllers\epayco\Utils\OpensslEncrypt;

/**
 * Epayco library encrypt based in AES
 */
if (function_exists('mcrypt_get_iv_size')) {

	class PaycoAes extends McryptEncrypt {}
}else{
	
	class PaycoAes extends OpensslEncrypt {}
}

?>