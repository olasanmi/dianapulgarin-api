<?php

namespace App\Http\Controllers;

use App\Counting;
use Illuminate\Http\Request;

use Response;

use App\Http\Resources\Counting\CountingCollection;

class CountingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $counting = Counting::orderBy('id', 'DESC')
        ->get();
        $response = Response::make(json_encode(['success' => new CountingCollection($counting)]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = Counting::where('service', '=', $request->service)->first();
        if ($validate) {
            return response()->json(['errors' => array(['code' => 422, 'message' => 'Existe un contador creado, por favor modifícalo.'])], 422);
        }

        Counting::create([
            'total' => $request->total,
            'title' => $request->title,
            'service' => $request->service
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Counting  $counting
     * @return \Illuminate\Http\Response
     */
    public function show(Counting $counting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Counting  $counting
     * @return \Illuminate\Http\Response
     */
    public function edit(Counting $counting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Counting  $counting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $counting = Counting::findOrFail($request->id);
        $counting->update([
            'total' => $request->total,
            'title' => $request->title,
            'service' => $request->service
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Counting  $counting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Counting $counting)
    {
        //
        $counting->delete();
    }
}
