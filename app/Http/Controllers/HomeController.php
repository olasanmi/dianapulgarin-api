<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Instagram\Api;
use Instagram\Exception\InstagramException;

use Psr\Cache\CacheException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    //Download images from remote server
    public function save_image($inPath,$outPath) {
        $in=    fopen($inPath, "rb");
        $out=   fopen($outPath, "wb");
        while ($chunk = fread($in,8192))
        {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);
    }

    public function getFeed() {
        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');
        $api = new Api($cachePool);
        $api->login('maalhequi95', '23678535Aa');
        $profile = $api->getProfile('dradianapulgarin');
        if (count($profile->getMedias()) > 0)
            $feedItems = $profile->getMedias();
        else
            $feedItems = [];

        $feedsAll = array();
        for ($i=0; $i <= 5; $i++) {
            $this->save_image($feedItems[$i]->getThumbnailSrc(), 'images/instagram/'. $i .'.jpg');
            $feedItems[$i]->img = $i. '.jpg';
            $feedItems[$i]->likess = $feedItems[$i]->getLikes();
            $feedItems[$i]->captions = $feedItems[$i]->getCaption();
            array_push($feedsAll, $feedItems[$i]);
        }
        return response()->json(['success' => $feedsAll]);
    }

}
