<?php

namespace App\Http\Controllers;

use App\CaseReal;
use Illuminate\Http\Request;

use Response;

use App\Helpers\Helper;

use App\Http\Resources\RealCase\RealCaseCollection;

class CaseRealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $realCases = CaseReal::orderBy('id', 'DESC')
        ->get();
        $response = Response::make(json_encode(['success' => new RealCaseCollection($realCases)]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $realCase = CaseReal::create([
                'description' => $request->description
            ]);
            if ($request->has('status')) {
                $realCase->status = $request->status;
            }
            if ($request->has('file')) {
                $path = Helper::uploadImage($request->file, 'caseReal');
                $realCase->path = $path;
            }
            if ($request->has('file2')) {
                $path2 = Helper::uploadImage($request->file2, 'caseReal');
                $realCase->comparative_path = $path2;
            }
            $realCase->save();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CaseReal  $caseReal
     * @return \Illuminate\Http\Response
     */
    public function show(CaseReal $caseReal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CaseReal  $caseReal
     * @return \Illuminate\Http\Response
     */
    public function edit(CaseReal $caseReal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CaseReal  $caseReal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try {
            $realCase = CaseReal::findOrFail($request->id);

            if ($request->has('file')) {
                Helper::deleteFile($realCase->path, 'caseReal');
                $path = Helper::uploadImage($request->file, 'caseReal');
                $realCase->path = $path;
            }
            if ($request->has('file2')) {
                Helper::deleteFile($realCase->comparative_path, 'caseReal');
                $path2 = Helper::uploadImage($request->file2, 'caseReal');
                $realCase->comparative_path = $path2;
            }

            if ($request->has('status')) {
                $realCase->status = $request->status;
            }

            $realCase->description = $request->description;
            $realCase->save();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CaseReal  $caseReal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $caseReal = CaseReal::find($id);

            if (isset($caseReal->path)) {
                Helper::deleteFile($caseReal->path, 'caseReal');
            }
            $caseReal->delete();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    public function getActives()
    {
        //
        try {
            $casesReal = CaseReal::where('status', '=', 1)
            ->orderBy('id', 'DESC')
            ->limit(16)
            ->get();
            $response = Response::make(json_encode(['success' => new RealCaseCollection($casesReal)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
