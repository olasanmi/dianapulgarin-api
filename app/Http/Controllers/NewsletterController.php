<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;

use Response;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $newsletters = Newsletter::orderBy('id', 'ASC')
        ->get();

        $response = Response::make(json_encode(['success' => $newsletters]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // validate email
            if (Newsletter::where('email', '=', $request->email)->count() > 0) {
               return response()->json(['errors' => array(['code' => 422, 'message' => 'El correo se encuentra registrado en nuestro newsletter.'])], 422);
            }
            //store
            $newsletter = Newsletter::create([
                'email' => $request->email,
                'phone' => $request->phone,
                'date_init'=> date("d/m/Y"),
                'name' => $request->name
            ]);

            if ($newsletter->id) {
                $response = Response::make(json_encode(['success' => 'Te has subsctrito correctamente a nuestro newsletter.']), 201)->header('Content-Type','application/json');
            }
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show(Newsletter $newsletter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function edit(Newsletter $newsletter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Newsletter $newsletter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newsletter $newsletter)
    {
        //
        try {
            $newsletter->delete();
            $response = $this->index();
            return $response;
            
        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
