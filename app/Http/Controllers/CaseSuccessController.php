<?php

namespace App\Http\Controllers;

use App\CaseSuccess;
use Illuminate\Http\Request;

use Response;

use App\Helpers\Helper;

use App\Http\Resources\RealCase\RealCaseCollection;

use DB;

class CaseSuccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $caseSuccess = CaseSuccess::orderBy('id', 'DESC')
        ->get();
        $response = Response::make(json_encode(['success' => new RealCaseCollection($caseSuccess)]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $caseSuccess = CaseSuccess::create([
                'description' => $request->description
            ]);
            if ($request->has('status')) {
                $caseSuccess->status = $request->status;
            }
            if ($request->has('file')) {
                $path = Helper::uploadImage($request->file, 'caseSuccess');
                $caseSuccess->path = $path;
            }
            if ($request->has('file2')) {
                $path2 = Helper::uploadImage($request->file2, 'caseSuccess');
                $caseSuccess->comparative_path = $path2;
            }
            $caseSuccess->save();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CaseSuccess  $caseSuccess
     * @return \Illuminate\Http\Response
     */
    public function show(CaseSuccess $caseSuccess)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CaseSuccess  $caseSuccess
     * @return \Illuminate\Http\Response
     */
    public function edit(CaseSuccess $caseSuccess)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CaseSuccess  $caseSuccess
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try {
            $caseSuccess = CaseSuccess::findOrFail($request->id);

            if ($request->has('file')) {
                Helper::deleteFile($caseSuccess->path, 'caseSuccess');
                $path = Helper::uploadImage($request->file, 'caseSuccess');
                $caseSuccess->path = $path;
            }
            if ($request->has('file2')) {
                Helper::deleteFile($caseSuccess->comparative_path, 'caseSuccess');
                $path2 = Helper::uploadImage($request->file2, 'caseSuccess');
                $caseSuccess->comparative_path = $path2;
            }

            if ($request->has('status')) {
                $caseSuccess->status = $request->status;
            }

            $caseSuccess->description = $request->description;
            $caseSuccess->save();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CaseSuccess  $caseSuccess
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $caseSuccess = CaseSuccess::find($id);

            if (isset($caseSuccess->path)) {
                Helper::deleteFile($caseSuccess->path, 'caseSuccess');
            }
            $caseSuccess->delete();

            $response = $this->index();
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    public function getActives()
    {
        //
        try {
            $casesSuccess = DB::table('case_successes')
            ->inRandomOrder()
            ->limit(12)
            ->get();
            $response = Response::make(json_encode(['success' => new RealCaseCollection($casesSuccess)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
