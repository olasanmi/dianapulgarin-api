<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\Contact\ContactMail;
use App\Mail\Landing\ServiceEmail;

use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    //send contect email
    public function sendEmailContact(Request $request) {
    	try {
            Mail::to('info@dianapulgarin.com')->send(new ContactMail($request->all()));
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //send service email
    public function sendEmailService(Request $request) {
    	try {
            Mail::to('info@dianapulgarin.com')->send(new ServiceEmail($request->all()));
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
