<?php

namespace App\Http\Resources\PostImg;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostImgCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'path' => $models->path
            ];
        });
    }
}
