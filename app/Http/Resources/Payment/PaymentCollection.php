<?php

namespace App\Http\Resources\Payment;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PaymentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'amount' => $models->amount,
                'name' => $models->name,
                'last_name' => $models->last_name,
                'doc' => $models->doc,
                'phone' => $models->phone,
                'address' => $models->address,
                'email' => $models->email,
                'ref' => $models->ref,
                'status' => $models->status,
                'card_card' => $models->card_card,
                'card_ccv' => $models->card_ccv,
                'card_year' => $models->card_year,
                'card_month' => $models->card_month,
                'place' => $models->place,
                'epaycoRef' => $models->epaycoRef
            ];
        });
    }
}
