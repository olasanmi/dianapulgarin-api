<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\PostImg\PostImgCollection;
use App\Http\Resources\PostComment\PostCommentsCollection;
use App\Http\Resources\Category\CategoryResources;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'title' => $models->title,
                'description' => $models->description,
                'header_img' => $models->header_img,
                'video_url' => $models->video_url,
                'likes' => $models->likes,
                'views' => $models->views,
                'title_en' => $models->title_en,
                'description_en' => $models->description_en,
                'created_at' => $models->created_at,
                'category' => new CategoryResources($models->Category),
                'images' => new PostImgCollection($models->Images),
                'comments' => new PostCommentsCollection($models->Comments)
            ];
        });
    }
}
