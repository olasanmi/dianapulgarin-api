<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PostImg\PostImgCollection;
use App\Http\Resources\PostComment\PostCommentsCollection;
use App\Http\Resources\Category\CategoryResources;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'header_img' => $this->header_img,
            'video_url' => $this->video_url,
            'likes' => $this->likes,
            'created_at' => $this->created_at,
            'views' => $this->views,
            'title_en' => $this->title_en,
            'description_en' => $this->description_en,
            'category' => new CategoryResources($this->Category),
            'images' => new PostImgCollection($this->Images),
            'comments' => new PostCommentsCollection($this->Comments)
        ];
    }
}
