<?php

namespace App\Http\Resources\RealCase;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RealCaseCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'description' => $models->description,
                'path' => $models->path,
                'status' => $models->status,
                'comparative_path' => $models->comparative_path
            ];
        });
    }
}
