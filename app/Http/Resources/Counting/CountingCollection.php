<?php

namespace App\Http\Resources\Counting;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CountingCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'title' => $models->title,
                'total' => $models->total,
                'service' => $models->service
            ];
        });
    }
}
