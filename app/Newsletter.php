<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    //fillable
    protected $fillable = ['email', 'phone', 'status', 'date_init', 'date_finish', 'name'];
}
