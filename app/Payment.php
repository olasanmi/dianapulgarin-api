<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //fillable
    protected $fillable = ['amount', 'name','last_name','doc','phone','address','email','ref','card_card','card_ccv','card_year','card_month', 'status', 'place'];
}
