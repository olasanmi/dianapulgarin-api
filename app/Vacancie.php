<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancie extends Model
{
    //fillable
    protected $fillable = ['name', 'status', 'sede'];
}
