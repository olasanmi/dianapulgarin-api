<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //fillable
    protected $fillable = ['title', 'description', 'header_img', 'video_url', 'is_hightlight', 'category_id', 'likes', 'views', 'title_en', 'description_en'];

    //relations
    public function Images() {
    	return $this->hasMany('App\PostImg');
    }

    public function Comments() {
    	return $this->hasMany('App\PostComment');
    }

    public function Category() {
    	return $this->belongsTo('App\Category');
    }
}
