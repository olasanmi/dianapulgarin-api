<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseReal extends Model
{
    //fillable
    protected $fillable = ['description', 'path', 'status', 'comparative_path'];
}
