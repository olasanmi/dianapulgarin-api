<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //fiilable
    protected $fillable = ['name', 'position'];

    //relations
    public function Post() {
    	return $this->hasMany('App\Post');
    }
}
