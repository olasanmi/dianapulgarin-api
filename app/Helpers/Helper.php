<?php
namespace App\Helpers;
use File;

use App\Domicilio;
use App\Mail\StatusChangeEmail;
use Illuminate\Support\Facades\Mail;

/**
 * 
 */
class Helper
{
	 //upload file
    public static function uploadImage($file, $pathModel) {
        $name = date('Ymd_His').rand(1, 100000000);
        $file->move('images/'.$pathModel, $name);
        return $name;
    }

    //delete file
    public static function deleteFile($file, $pathModel) {
        $oldfile = public_path('images/'.$pathModel.'/'.$file);
        if (File::exists($oldfile)) {
            unlink($oldfile);
        }
    }

    //add reference
    public static function generateReference($length) {
        $key = '';
        $keys = array_merge(range(0, 6), range('A', 'Z'));
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }
    
}