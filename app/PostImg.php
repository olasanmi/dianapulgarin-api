<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImg extends Model
{
    //fillable
    protected $fillable = ['path', 'post_id'];

    //relations
    public function Images() {
    	return $this->belongsTo('App\Post');
    }
}
