<?php

use Illuminate\Database\Seeder;

class OtherUserSedders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'administracionmedellin',
            'email' => 'administracionmedellin@dianapulgarin.com@admin.co',
            'password' => bcrypt('administracionmedellin2021-')
        ]);
        DB::table('users')->insert([
            'name' => 'auxcontablemedellin',
            'email' => 'auxcontablemedellin@dianapulgarin.com',
            'password' => bcrypt('auxcontablemedellin2021-')
        ]);
    }
}
